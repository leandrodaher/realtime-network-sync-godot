extends Control

const SERVER_PORT = 5001
const MAX_PLAYERS = 4
var SERVER_IP = "127.0.0.1"
onready var log_label = $CenterContainer/GridContainer/Log

func _ready():
	get_tree().connect("network_peer_connected", self, "_player_connected")
	get_tree().connect("network_peer_disconnected", self, "_player_disconnected")
	get_tree().connect("connected_to_server", self, "_connected_to_server")
	get_tree().connect("connection_failed", self, "_connection_failed")
	get_tree().connect("server_disconnected", self, "_server_disconnected")

func _player_connected(id):
	print("player ",id," connected")
	log_label.text += str("\nplayer ",id," connected")
	game_manager.player_id = id

func _player_disconnected(id):
	print("player ",id," disconnected")
	log_label.text += str("\nplayer ",id," disconnected")
	

func _connected_to_server():
	print("_connected_to_server")
	log_label.text += "\n_connected_to_server"
	rpc_id(1,"start_demo")
	

func _connection_failed():
	print("_connection_failed")
	log_label.text += "\n_connection_failed"

func _server_disconnected():
	print("_server_disconnected")
	log_label.text += "\n_server_disconnected"

# Signals from buttons
func _on_PeerAsServer_pressed():
	var peer = NetworkedMultiplayerENet.new()
	peer.create_server(SERVER_PORT, MAX_PLAYERS)
	get_tree().set_network_peer(peer)
	
	#Prevent the server to become client
	$CenterContainer/GridContainer/PeerAsClient.disabled = true
	
	log_label.text += "Server initialized"

func _on_PeerAsClient_pressed():
	var peer = NetworkedMultiplayerENet.new()
	peer.create_client(SERVER_IP, SERVER_PORT)
	get_tree().set_network_peer(peer)
	log_label.text += "\nClient initialized"


remote func start_demo():
	game_manager.start_demo()
