# A cube designed to be pushed.

extends RigidBody

var active_material = preload("res://active.material")
var sleeping_material = preload("res://sleep.material")
sync var server_transform = Transform()

var interpolation_active = true

func _ready():
	if !sleeping:
		$MeshInstance.set_surface_material(0,active_material)
	if !is_network_master():
		server_transform = transform #Try commenting this to see what happens :P
#		mode = MODE_KINEMATIC
	
func _physics_process(delta):
	if is_network_master():
		rset_unreliable('server_transform',transform)
	else:
		if interpolation_active:
			# Interpolates the transform. 
			# We are sending packets 60 times per second
			# But most likely we won't receive 60 packets per second, some packets may get lost
			# some may reach at the same frame
			# So I tried to do this "by the book" andcouldn't figure it out completely,instead
			# I apply an interpolation based on the distance		
			var scale_factor = 0.1
			var dist = transform.origin.distance_squared_to(server_transform.origin)
			var weight = clamp(pow(2,dist/4)*scale_factor,0.0,1.0)
			transform = transform.interpolate_with(server_transform,weight)
		else:
			transform = server_transform
			
puppet func push(impulse):
	apply_impulse(Vector3.ZERO,impulse)
	# Only applies the impulse if network master. In this case the server.
	# The clients(puppets) interpolate
	if is_network_master():
		rpc_unreliable('push',impulse)

# When the rigidbody is active, asign a blue material. When sleeping asign a red material
# We are not sending this so each client change its color, this is noticeable longer on the client
# than the server, as it is performing interpolations, so even if the object seem quiet it is moving 
func _on_RigidBody_sleeping_state_changed():
	if !sleeping:
		$MeshInstance.set_surface_material(0,active_material)
	else: 
		$MeshInstance.set_surface_material(0,sleeping_material)

func toggle_interpolation():
	interpolation_active = !interpolation_active